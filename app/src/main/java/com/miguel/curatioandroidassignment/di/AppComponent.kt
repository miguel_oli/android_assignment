package com.miguel.curatioandroidassignment.di

import com.miguel.core.di.CoreModule
import com.miguel.curatioandroidassignment.MainApplication
import com.miguel.onboarding.di.OnboardingModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, CoreModule::class, OnboardingModule::class])
interface AppComponent: AndroidInjector<MainApplication>