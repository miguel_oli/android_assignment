package com.miguel.curatioandroidassignment

import com.miguel.core.di.CoreModule
import com.miguel.curatioandroidassignment.di.AppComponent
import com.miguel.curatioandroidassignment.di.DaggerAppComponent
import com.miguel.onboarding.di.OnboardingModule
import dagger.android.support.DaggerApplication

class MainApplication: DaggerApplication() {

    private val appComponent: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .coreModule(CoreModule(this))
            .onboardingModule(OnboardingModule())
            .build()
    }

    override fun applicationInjector() = appComponent

}