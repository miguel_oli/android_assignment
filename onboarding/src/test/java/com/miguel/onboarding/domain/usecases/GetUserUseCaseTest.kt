package com.miguel.onboarding.domain.usecases

import com.miguel.core.domain.models.UserModel
import com.miguel.core.domain.repositories.contract.ILocalUserRepository
import com.miguel.core.domain.repositories.result.ResultUserRepository
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test

internal class GetUserUseCaseTest {

    private lateinit var repository: ILocalUserRepository
    private lateinit var useCase: GetUserUseCase

    @Before
    fun setUp() {
        repository = mockk()
        useCase = GetUserUseCase(repository)
    }

    @Test
    fun `GetUserUseCase with success result`() = runBlocking {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        coEvery { repository.get() } returns ResultUserRepository.Success(user)

        val result = useCase()

        coVerify { repository.get() }
        assertThat(result, instanceOf(ResultUserUseCase.Success::class.java))
        assertEquals(user, (result as ResultUserUseCase.Success).model)
    }

    @Test
    fun `GetUserUseCase with user not found result`() = runBlocking {
        coEvery { repository.get() } returns ResultUserRepository.UserNotFound

        val result = useCase()

        coVerify { repository.get() }
        assertEquals(result, ResultUserUseCase.UserNotFound)
    }

    @Test
    fun `GetUserUseCase with error result`() = runBlocking {
        val message = "generic error"
        coEvery { repository.get() } returns ResultUserRepository.Error(message)

        val result = useCase()

        coVerify { repository.get() }
        assertThat(result, instanceOf(ResultUserUseCase.Error::class.java))
        assertEquals(message, (result as ResultUserUseCase.Error).message)
    }

}