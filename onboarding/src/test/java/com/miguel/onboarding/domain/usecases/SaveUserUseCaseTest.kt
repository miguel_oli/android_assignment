package com.miguel.onboarding.domain.usecases

import com.miguel.core.domain.models.UserModel
import com.miguel.core.domain.repositories.contract.ILocalUserRepository
import com.miguel.core.domain.repositories.result.ResultUserRepository
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test

internal class SaveUserUseCaseTest {

    private lateinit var repository: ILocalUserRepository
    private lateinit var useCase: SaveUserUseCase

    @Before
    fun setUp() {
        repository = mockk()
        useCase = SaveUserUseCase(repository)
    }

    @Test
    fun `SaveUserUseCase with success result`() = runBlocking {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        coEvery { repository.save(user) } returns ResultUserRepository.Success(user)

        val result = useCase(user)

        coVerify { repository.save(user) }
        Assert.assertThat(result, CoreMatchers.instanceOf(ResultUserUseCase.Success::class.java))
        Assert.assertEquals(user, (result as ResultUserUseCase.Success).model)
    }

    @Test
    fun `SaveUserUseCase with user not found result`() = runBlocking {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        coEvery { repository.save(user) } returns ResultUserRepository.UserNotFound

        val result = useCase(user)

        coVerify { repository.save(user) }
        Assert.assertEquals(result, ResultUserUseCase.UserNotFound)
    }

    @Test
    fun `SaveUserUseCase with error result`() = runBlocking {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        val message = "generic error"
        coEvery { repository.save(user) } returns ResultUserRepository.Error(message)

        val result = useCase(user)

        coVerify { repository.save(user) }
        Assert.assertThat(result, CoreMatchers.instanceOf(ResultUserUseCase.Error::class.java))
        Assert.assertEquals(message, (result as ResultUserUseCase.Error).message)
    }

}