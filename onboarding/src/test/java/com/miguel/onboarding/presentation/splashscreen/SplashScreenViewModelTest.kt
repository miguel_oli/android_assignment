package com.miguel.onboarding.presentation.splashscreen

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import com.miguel.onboarding.testutils.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O])
internal class SplashScreenViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var getUserUseCase: GetUserUseCase
    private lateinit var viewModel: SplashScreenViewModel

    @Before
    fun setUp() {
        getUserUseCase = mockk()
        viewModel = SplashScreenViewModel(getUserUseCase)
    }

    @Test
    fun `retrieveUser with success result`() {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        coEvery { getUserUseCase() } returns ResultUserUseCase.Success(user)

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(result, instanceOf(SplashScreenViewModel.SplashScreenStates.Success::class.java))
        assertEquals(user, (result as SplashScreenViewModel.SplashScreenStates.Success).model)
    }

    @Test
    fun `retrieveUser with user not found result`() {
        coEvery { getUserUseCase() } returns ResultUserUseCase.UserNotFound

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(result, instanceOf(SplashScreenViewModel.SplashScreenStates.UserNotFound::class.java))
    }

    @Test
    fun `retrieveUser with error result`() {
        val message = "generic error"
        coEvery { getUserUseCase() } returns ResultUserUseCase.Error(message)

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(result, instanceOf(SplashScreenViewModel.SplashScreenStates.Error::class.java))
        assertEquals(message, (result as SplashScreenViewModel.SplashScreenStates.Error).message)
    }

}