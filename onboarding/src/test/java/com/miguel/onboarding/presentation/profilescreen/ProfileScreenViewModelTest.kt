package com.miguel.onboarding.presentation.profilescreen

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import com.miguel.onboarding.testutils.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O])
class ProfileScreenViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var getUserUseCase: GetUserUseCase
    private lateinit var viewModel: ProfileScreenViewModel

    @Before
    fun setUp() {
        getUserUseCase = mockk()
        viewModel = ProfileScreenViewModel(getUserUseCase)
    }

    @Test
    fun `retrieveUser with success result`() {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        coEvery { getUserUseCase() } returns ResultUserUseCase.Success(user)

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(result, instanceOf(ProfileScreenViewModel.ProfileScreenStates.Success::class.java))
        assertEquals(user, (result as ProfileScreenViewModel.ProfileScreenStates.Success).model)
    }

    @Test
    fun `retrieveUser with user not found result`() {
        coEvery { getUserUseCase() } returns ResultUserUseCase.UserNotFound

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(result, instanceOf(ProfileScreenViewModel.ProfileScreenStates.UserNotFound::class.java))
    }

    @Test
    fun `retrieveUser with error result`() {
        val message = "generic error"
        coEvery { getUserUseCase() } returns ResultUserUseCase.Error(message)

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(result, instanceOf(ProfileScreenViewModel.ProfileScreenStates.Error::class.java))
        assertEquals(message, (result as ProfileScreenViewModel.ProfileScreenStates.Error).message)
    }

}