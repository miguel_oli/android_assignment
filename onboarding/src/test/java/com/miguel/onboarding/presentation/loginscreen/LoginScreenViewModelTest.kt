package com.miguel.onboarding.presentation.loginscreen

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.domain.usecases.SaveUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import com.miguel.onboarding.testutils.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O])
internal class LoginScreenViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var saveUserUseCase: SaveUserUseCase
    private lateinit var viewModel: LoginScreenViewModel

    @Before
    fun setUp() {
        saveUserUseCase = mockk()
        viewModel = LoginScreenViewModel(saveUserUseCase)
    }

    @Test
    fun `saveUser with success result`() {
        val user = UserModel(
            email = "teste@gmail.com",
            password = "123456",
        )
        coEvery { saveUserUseCase(user) } returns ResultUserUseCase.Success(user)

        viewModel.saveUser(user.email, user.password)
        val result = viewModel.saveUser.getOrAwaitValue()

        coVerify { saveUserUseCase(user) }
        assertThat(
            result,
            instanceOf(LoginScreenViewModel.LoginScreenStates.Success::class.java)
        )
        assertEquals(
            user,
            (result as LoginScreenViewModel.LoginScreenStates.Success).model
        )
    }

    @Test
    fun `saveUser with error result`() {
        val user = UserModel(
            email = "teste@gmail.com",
            password = "123456",
        )
        val message = "generic error"
        coEvery { saveUserUseCase(user) } returns ResultUserUseCase.Error(message)

        viewModel.saveUser(user.email, user.password)
        val result = viewModel.saveUser.getOrAwaitValue()

        coVerify { saveUserUseCase(user) }
        assertThat(
            result,
            instanceOf(LoginScreenViewModel.LoginScreenStates.Error::class.java)
        )
        assertEquals(
            message,
            (result as LoginScreenViewModel.LoginScreenStates.Error).message
        )
    }

    @Test
    fun `success validation for e-mail`() {
        val email = "teste@gmail.com"
        viewModel.validateEmail(email)
        val result = viewModel.messageValidationEmail.getOrAwaitValue()

        assertNull(result)
    }

    @Test
    fun `validation for empty e-mail`() {
        val email = ""
        viewModel.validateEmail(email)
        val result = viewModel.messageValidationEmail.getOrAwaitValue()

        assertNotNull(result)
    }

    @Test
    fun `validation for incomplete e-mail`() {
        val emails = listOf(
            "teste",
            "teste@",
            "teste@gmail",
            "teste@gmail.",
        )
        emails.forEach { email ->
            viewModel.validateEmail(email)
            val result = viewModel.messageValidationEmail.getOrAwaitValue()

            assertNotNull(result)
        }
    }

    @Test
    fun `success validation for password`() {
        val password = "123456"
        viewModel.validatePassword(password)
        val result = viewModel.messageValidationPassword.getOrAwaitValue()

        assertNull(result)
    }

    @Test
    fun `validation for password with less than 6 characters`() {
        val password = "12345"
        viewModel.validatePassword(password)
        val result = viewModel.messageValidationPassword.getOrAwaitValue()

        assertNotNull(result)
    }

}