package com.miguel.onboarding.presentation.onboardingscreen

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.SaveUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import com.miguel.onboarding.testutils.getOrAwaitValue
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O])
class OnboardingScreenViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var getUserUseCase: GetUserUseCase
    private lateinit var saveUserUseCase: SaveUserUseCase
    private lateinit var viewModel: OnboardingScreenViewModel

    @Before
    fun setUp() {
        getUserUseCase = mockk()
        saveUserUseCase = mockk()
        viewModel = OnboardingScreenViewModel(getUserUseCase, saveUserUseCase)
    }

    @Test
    fun `retrieveUser with success result`() {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        coEvery { getUserUseCase() } returns ResultUserUseCase.Success(user)

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(
            result,
            instanceOf(OnboardingScreenViewModel.OnboardingScreenStates.Success::class.java)
        )
        assertEquals(
            user,
            (result as OnboardingScreenViewModel.OnboardingScreenStates.Success).model
        )
    }

    @Test
    fun `retrieveUser with user not found result`() {
        coEvery { getUserUseCase() } returns ResultUserUseCase.UserNotFound

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(
            result,
            instanceOf(OnboardingScreenViewModel.OnboardingScreenStates.UserNotFound::class.java)
        )
    }

    @Test
    fun `retrieveUser with error result`() {
        val message = "generic error"
        coEvery { getUserUseCase() } returns ResultUserUseCase.Error(message)

        viewModel.getUser()
        val result = viewModel.retrieveUser.getOrAwaitValue()

        coVerify { getUserUseCase() }
        assertThat(
            result,
            instanceOf(OnboardingScreenViewModel.OnboardingScreenStates.Error::class.java)
        )
        assertEquals(
            message,
            (result as OnboardingScreenViewModel.OnboardingScreenStates.Error).message
        )
    }

    @Test
    fun `saveUser with success result`() {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "",
            hasDoneOnboarding = true,
        )
        coEvery { saveUserUseCase(user) } returns ResultUserUseCase.Success(user)

        viewModel.saveUser(
            firstName = "José",
            lastName = "Silva",
            age = "25",
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
        )
        val result = viewModel.saveUser.getOrAwaitValue()

        coVerify { saveUserUseCase(user) }
        assertThat(
            result,
            instanceOf(OnboardingScreenViewModel.OnboardingScreenStates.Success::class.java)
        )
        assertEquals(
            user,
            (result as OnboardingScreenViewModel.OnboardingScreenStates.Success).model
        )
    }

    @Test
    fun `saveUser with error result`() {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "",
            hasDoneOnboarding = true,
        )
        val message = "generic error"
        coEvery { saveUserUseCase(user) } returns ResultUserUseCase.Error(message)

        viewModel.saveUser(
            firstName = "José",
            lastName = "Silva",
            age = "25",
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
        )
        val result = viewModel.saveUser.getOrAwaitValue()

        coVerify { saveUserUseCase(user) }
        assertThat(
            result,
            instanceOf(OnboardingScreenViewModel.OnboardingScreenStates.Error::class.java)
        )
        assertEquals(
            message,
            (result as OnboardingScreenViewModel.OnboardingScreenStates.Error).message
        )
    }

    @Test
    fun `success validation for e-mail`() {
        val email = "teste@gmail.com"
        viewModel.validateEmail(email)
        val result = viewModel.messageValidationEmail.getOrAwaitValue()

        assertNull(result)
    }

    @Test
    fun `validation for empty e-mail`() {
        val email = ""
        viewModel.validateEmail(email)
        val result = viewModel.messageValidationEmail.getOrAwaitValue()

        assertNotNull(result)
    }

    @Test
    fun `validation for incomplete e-mail`() {
        val emails = listOf(
            "teste",
            "teste@",
            "teste@gmail",
            "teste@gmail.",
        )
        emails.forEach { email ->
            viewModel.validateEmail(email)
            val result = viewModel.messageValidationEmail.getOrAwaitValue()

            assertNotNull(result)
        }
    }

    @Test
    fun `success validation for first name`() {
        val firstName = "José"
        viewModel.validateFirstName(firstName)
        val result = viewModel.messageValidationFirstName.getOrAwaitValue()

        assertNull(result)
    }

    @Test
    fun `validation for empty first name`() {
        val firstName = ""
        viewModel.validateFirstName(firstName)
        val result = viewModel.messageValidationFirstName.getOrAwaitValue()

        assertNotNull(result)
    }

    @Test
    fun `validation for incomplete first name`() {
        val firstNames = listOf(
            "J",
            "Jo",
        )
        firstNames.forEach { firstName ->
            viewModel.validateFirstName(firstName)
            val result = viewModel.messageValidationFirstName.getOrAwaitValue()

            assertNotNull(result)
        }
    }

    @Test
    fun `success validation for last name`() {
        val lastName = "Silva"
        viewModel.validateLastName(lastName)
        val result = viewModel.messageValidationLastName.getOrAwaitValue()

        assertNull(result)
    }

    @Test
    fun `validation for empty last name`() {
        val lastName = ""
        viewModel.validateLastName(lastName)
        val result = viewModel.messageValidationLastName.getOrAwaitValue()

        assertNotNull(result)
    }

    @Test
    fun `validation for incomplete last name`() {
        val lastNames = listOf(
            "S",
            "Si",
        )
        lastNames.forEach { lastName ->
            viewModel.validateLastName(lastName)
            val result = viewModel.messageValidationLastName.getOrAwaitValue()

            assertNotNull(result)
        }
    }

    @Test
    fun `success validation for gender`() {
        val gender = "Male"
        viewModel.validateGender(gender)
        val result = viewModel.messageValidationGender.getOrAwaitValue()

        assertNull(result)
    }

    @Test
    fun `validation for empty gender`() {
        val gender = ""
        viewModel.validateGender(gender)
        val result = viewModel.messageValidationGender.getOrAwaitValue()

        assertNotNull(result)
    }

    @Test
    fun `validation for incomplete gender`() {
        val genders = listOf(
            "M",
            "Ma",
            "Mal",
        )
        genders.forEach { gender ->
            viewModel.validateGender(gender)
            val result = viewModel.messageValidationGender.getOrAwaitValue()

            assertNotNull(result)
        }
    }

    @Test
    fun `success validation for age`() {
        val age = "25"
        viewModel.validateAge(age)
        val result = viewModel.messageValidationAge.getOrAwaitValue()

        assertNull(result)
    }

    @Test
    fun `validation for empty age`() {
        val age = ""
        viewModel.validateAge(age)
        val result = viewModel.messageValidationAge.getOrAwaitValue()

        assertNotNull(result)
    }

    @Test
    fun `validation for invalid age`() {
        val ages = listOf(
            "0",
            "-1",
        )
        ages.forEach { age ->
            viewModel.validateAge(age)
            val result = viewModel.messageValidationAge.getOrAwaitValue()

            assertNotNull(result)
        }
    }

}