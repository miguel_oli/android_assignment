package com.miguel.onboarding.presentation.onboardingscreen

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.R
import com.miguel.onboarding.databinding.OnboardingScreenFragmentBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class OnboardingScreenFragment : DaggerFragment() {

    @Inject
    lateinit var viewModel: OnboardingScreenViewModel

    private lateinit var _binding: OnboardingScreenFragmentBinding
    private val binding get() = _binding

    private val navController: NavController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        _binding = OnboardingScreenFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarListener()
        firstNameValidation()
        lastNameValidation()
        ageValidation()
        genderValidation()
        emailValidation()
        retrieveUser()
        saveUser()
    }

    private fun toolbarListener() {
        binding.toolbar.setOnMenuItemClickListener { item ->
            when(item.itemId) {
                R.id.it_save -> saveClick()
                R.id.it_cancel -> cancelClick()
                else -> super.onOptionsItemSelected(item)
            }
        }
    }

    private fun firstNameValidation() {
        validateFirstName()
        observeFirstNameValidation()
    }

    private fun validateFirstName() =
        binding.firstNameInputText.doOnTextChanged { text, _, _, _ ->
            viewModel.validateFirstName(text.toString())
        }

    private fun observeFirstNameValidation() =
        viewModel.messageValidationFirstName.observe(viewLifecycleOwner) {
            binding.firstNameInputLayout.error = it
        }

    private fun lastNameValidation() {
        validateLastName()
        observeLastNameValidation()
    }

    private fun validateLastName() =
        binding.lastNameInputText.doOnTextChanged { text, _, _, _ ->
            viewModel.validateLastName(text.toString())
        }

    private fun observeLastNameValidation() =
        viewModel.messageValidationLastName.observe(viewLifecycleOwner) {
            binding.lastNameInputLayout.error = it
        }

    private fun ageValidation() {
        validateAge()
        observeAgeValidation()
    }

    private fun validateAge() =
        binding.ageInputText.doOnTextChanged { text, _, _, _ ->
            viewModel.validateAge(text.toString())
        }

    private fun observeAgeValidation() =
        viewModel.messageValidationAge.observe(viewLifecycleOwner) {
            binding.ageInputLayout.error = it
        }

    private fun genderValidation() {
        validateGender()
        observeGenderValidation()
    }

    private fun validateGender() =
        binding.genderInputText.doOnTextChanged { text, _, _, _ ->
            viewModel.validateGender(text.toString())
        }

    private fun observeGenderValidation() =
        viewModel.messageValidationGender.observe(viewLifecycleOwner) {
            binding.genderInputLayout.error = it
        }

    private fun emailValidation() {
        validateEmail()
        observeEmailValidation()
    }

    private fun validateEmail() =
        binding.emailInputText.doOnTextChanged { text, _, _, _ ->
            viewModel.validateEmail(text.toString())
        }

    private fun observeEmailValidation() =
        viewModel.messageValidationEmail.observe(viewLifecycleOwner) {
            binding.emailInputLayout.error = it
        }

    private fun retrieveUser() {
        getUser()
        observeRetrieveUser()
    }

    private fun getUser() = viewModel.getUser()

    private fun observeRetrieveUser() = viewModel.retrieveUser.observe(viewLifecycleOwner) { viewState ->
        when(viewState) {
            is OnboardingScreenViewModel.OnboardingScreenStates.Error -> {
                showMessage(viewState.message)
                navigateToLogin()
            }
            is OnboardingScreenViewModel.OnboardingScreenStates.Success -> fillFieldsWithUser(viewState.model)
            OnboardingScreenViewModel.OnboardingScreenStates.UserNotFound -> {
                showMessage("User not found")
                navigateToLogin()
            }
        }
    }

    private fun saveUser() {
        observeSaveUser()
    }

    private fun observeSaveUser() {
        viewModel.saveUser.observe(viewLifecycleOwner) { viewState ->
            when(viewState) {
                is OnboardingScreenViewModel.OnboardingScreenStates.Error -> showMessage(viewState.message)
                is OnboardingScreenViewModel.OnboardingScreenStates.Success -> navigateToProfileScreen()
                OnboardingScreenViewModel.OnboardingScreenStates.UserNotFound -> showMessage("User not found")
            }
        }
    }

    private fun saveClick(): Boolean {
        binding.apply {
            viewModel.saveUser(
                firstNameInputText.text.toString(),
                lastNameInputText.text.toString(),
                ageInputText.text.toString(),
                genderInputText.text.toString(),
                countryInputText.text.toString(),
                emailInputText.text.toString(),
            )
        }
        return true
    }

    private fun cancelClick(): Boolean {
        navigateToProfileScreen()
        return true
    }

    private fun fillFieldsWithUser(user: UserModel) = binding.apply {
        val age = if(user.age > 0) user.age.toString() else ""
        
        firstNameInputText.setText(user.firstName)
        lastNameInputText.setText(user.lastName)
        ageInputText.setText(age)
        genderInputText.setText(user.gender)
        countryInputText.setText(user.country)
        emailInputText.setText(user.email)
    }

    private fun showMessage(message: String) = Toast.makeText(
        requireContext(),
        message,
        Toast.LENGTH_SHORT
    ).show()

    private fun navigateToLogin() =
        navController.navigate(R.id.action_onboardingScreenFragment_to_loginScreenFragment)

    private fun navigateToProfileScreen() {
        navController.navigate(R.id.action_onboardingScreenFragment_to_profileScreenFragment)
    }

}