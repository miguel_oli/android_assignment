package com.miguel.onboarding.presentation.profilescreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileScreenViewModel
    @Inject constructor(private val getUserUseCase: GetUserUseCase)
        : ViewModel() {

    sealed class ProfileScreenStates {
        class Success(val model: UserModel): ProfileScreenStates()
        class Error(val message: String): ProfileScreenStates()
        object UserNotFound: ProfileScreenStates()
    }

    private val _retrieveUser = MutableLiveData<ProfileScreenStates>()
    val retrieveUser: LiveData<ProfileScreenStates> get() = _retrieveUser

    fun getUser() = viewModelScope.launch {
        _retrieveUser.value = when(val result = getUserUseCase()) {
            is ResultUserUseCase.Error -> ProfileScreenStates.Error(result.message)
            is ResultUserUseCase.Success -> ProfileScreenStates.Success(result.model)
            ResultUserUseCase.UserNotFound -> ProfileScreenStates.UserNotFound
        }
    }

}
