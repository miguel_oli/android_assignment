package com.miguel.onboarding.presentation.loginscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.R
import com.miguel.onboarding.databinding.LoginScreenFragmentBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class LoginScreenFragment : DaggerFragment() {

    @Inject
    lateinit var viewModel: LoginScreenViewModel

    private lateinit var _binding: LoginScreenFragmentBinding
    private val binding get() = _binding

    private val navController: NavController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LoginScreenFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        emailValidation()
        passwordValidation()

        saveUser()
    }

    private fun emailValidation() {
        validateEmail()
        observeEmailValidation()
    }

    private fun validateEmail() =
        binding.emailInputText.doOnTextChanged { text, _, _, _ ->
             viewModel.validateEmail(text.toString())
        }

    private fun observeEmailValidation() =
        viewModel.messageValidationEmail.observe(viewLifecycleOwner) {
            binding.emailInputLayout.error = it
        }

    private fun passwordValidation() {
        validatePassword()
        observePasswordValidation()
    }

    private fun validatePassword() =
        binding.passwordInputText.doOnTextChanged { text, _, _, _ ->
            viewModel.validatePassword(text.toString())
        }

    private fun observePasswordValidation() =
        viewModel.messageValidationPassword.observe(viewLifecycleOwner) {
            binding.passwordInputLayout.error = it
        }

    private fun saveUser() {
        loginClickListener()
        observeSaveUser()
    }

    private fun loginClickListener() = binding.loginButton.setOnClickListener {
        viewModel.saveUser(
            binding.emailInputText.text.toString(),
            binding.passwordInputText.text.toString(),
        )
    }

    private fun observeSaveUser() = viewModel.saveUser.observe(viewLifecycleOwner) { viewState ->
        when(viewState) {
            is LoginScreenViewModel.LoginScreenStates.Error -> showMessage(viewState.message)
            is LoginScreenViewModel.LoginScreenStates.Success -> navigateToOnboardingOrProfileScreen(viewState.model)
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    private fun navigateToOnboardingOrProfileScreen(user: UserModel) {
        if(user.hasDoneOnboarding) {
            navController.navigate(R.id.action_loginScreenFragment_to_profileScreenFragment)
        } else {
            navController.navigate(R.id.action_loginScreenFragment_to_onboardingScreenFragment)
        }
    }

}