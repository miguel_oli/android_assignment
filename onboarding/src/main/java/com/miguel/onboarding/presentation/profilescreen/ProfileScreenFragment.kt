package com.miguel.onboarding.presentation.profilescreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.R
import com.miguel.onboarding.databinding.ProfileScreenFragmentBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ProfileScreenFragment : DaggerFragment() {

    @Inject
    lateinit var viewModel: ProfileScreenViewModel

    private lateinit var _binding: ProfileScreenFragmentBinding
    private val binding get() = _binding

    private val navController: NavController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ProfileScreenFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarListener()
        retrieveUser()
    }

    private fun toolbarListener() {
        binding.toolbar.setOnMenuItemClickListener { item ->
            when(item.itemId) {
                R.id.it_edit -> editClick()
                else -> super.onOptionsItemSelected(item)
            }
        }
    }

    private fun retrieveUser() {
        getUser()
        observeRetrieveUser()
    }

    private fun getUser() = viewModel.getUser()

    private fun observeRetrieveUser() = viewModel.retrieveUser.observe(viewLifecycleOwner) { viewState ->
        when(viewState) {
            is ProfileScreenViewModel.ProfileScreenStates.Error -> {
                showMessage(viewState.message)
                navigateToLogin()
            }
            is ProfileScreenViewModel.ProfileScreenStates.Success -> fillFieldsWithUser(viewState.model)
            ProfileScreenViewModel.ProfileScreenStates.UserNotFound -> {
                showMessage("User not found")
                navigateToLogin()
            }
        }
    }

    private fun showMessage(message: String) = Toast.makeText(
        requireContext(),
        message,
        Toast.LENGTH_SHORT
    ).show()

    private fun fillFieldsWithUser(user: UserModel) = binding.apply {
        val age = if(user.age > 0) user.age.toString() else ""

        firstNameInputText.setText(user.firstName)
        lastNameInputText.setText(user.lastName)
        ageInputText.setText(age)
        genderInputText.setText(user.gender)
        countryInputText.setText(user.country)
        emailInputText.setText(user.email)
    }

    private fun editClick(): Boolean {
        navigateToOnboarding()
        return true
    }

    private fun navigateToLogin() =
        navController.navigate(R.id.action_profileScreenFragment_to_loginScreenFragment)

    private fun navigateToOnboarding() =
        navController.navigate(R.id.action_profileScreenFragment_to_onboardingScreenFragment)

}