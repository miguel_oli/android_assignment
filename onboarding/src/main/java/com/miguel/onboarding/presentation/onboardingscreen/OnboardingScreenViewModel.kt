package com.miguel.onboarding.presentation.onboardingscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miguel.core.domain.models.UserModel
import com.miguel.core.extensions.*
import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.SaveUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

class OnboardingScreenViewModel
    @Inject constructor(
        private val getUserUseCase: GetUserUseCase,
        private val saveUserUseCase: SaveUserUseCase,
    ): ViewModel() {

    sealed class OnboardingScreenStates {
        class Success(val model: UserModel): OnboardingScreenStates()
        class Error(val message: String): OnboardingScreenStates()
        object UserNotFound: OnboardingScreenStates()
    }

    private val _retrieveUser = MutableLiveData<OnboardingScreenStates>()
    val retrieveUser: LiveData<OnboardingScreenStates> get() = _retrieveUser

    private val _saveUser = MutableLiveData<OnboardingScreenStates>()
    val saveUser: LiveData<OnboardingScreenStates> get() = _saveUser

    private val _messageValidationFirstName = MutableLiveData<String?>()
    val messageValidationFirstName: LiveData<String?> get() = _messageValidationFirstName

    private val _messageValidationLastName = MutableLiveData<String?>()
    val messageValidationLastName: LiveData<String?> get() = _messageValidationLastName

    private val _messageValidationAge = MutableLiveData<String?>()
    val messageValidationAge: LiveData<String?> get() = _messageValidationAge

    private val _messageValidationGender = MutableLiveData<String?>()
    val messageValidationGender: LiveData<String?> get() = _messageValidationGender

    private val _messageValidationEmail = MutableLiveData<String?>()
    val messageValidationEmail: LiveData<String?> get() = _messageValidationEmail

    private var _userModel = UserModel()

    fun getUser() = viewModelScope.launch {
        _retrieveUser.value = when(val result = getUserUseCase()) {
            is ResultUserUseCase.Error -> OnboardingScreenStates.Error(result.message)
            is ResultUserUseCase.Success -> {
                _userModel = result.model
                OnboardingScreenStates.Success(_userModel)
            }
            ResultUserUseCase.UserNotFound -> OnboardingScreenStates.UserNotFound
        }
    }

    fun saveUser(
        firstName: String,
        lastName: String,
        age: String,
        gender: String,
        country: String,
        email: String,
    ) = viewModelScope.launch {
        val isAllFieldsValid = validateFields(
            firstName = firstName,
            lastName = lastName,
            age = age,
            gender = gender,
            email = email,
        )

        if(isAllFieldsValid) {
            val user = UserModel(
                firstName = firstName,
                lastName = lastName,
                age = age.toIntOrNull() ?: 0,
                gender = gender,
                country = country,
                email = email,
                password = _userModel.password,
                hasDoneOnboarding = true,
            )

            _saveUser.value = when (val result = saveUserUseCase(user)) {
                is ResultUserUseCase.Error -> OnboardingScreenStates.Error(result.message)
                is ResultUserUseCase.Success -> OnboardingScreenStates.Success(result.model)
                ResultUserUseCase.UserNotFound -> OnboardingScreenStates.UserNotFound
            }
        }
    }

    private fun validateFields(
        firstName: String,
        lastName: String,
        age: String,
        gender: String,
        email: String,
    ): Boolean {
        validateFirstName(firstName)
        validateLastName(lastName)
        validateAge(age)
        validateGender(gender)
        validateEmail(email)

        return _messageValidationFirstName.value == null
                && _messageValidationLastName.value == null
                && _messageValidationAge.value == null
                && _messageValidationGender.value == null
                && _messageValidationEmail.value == null
    }

    fun validateFirstName(firstName: String) {
        _messageValidationFirstName.value = firstName.isFirstNameValid()
    }

    fun validateLastName(lastName: String) {
        _messageValidationLastName.value = lastName.isLastNameValid()
    }

    fun validateAge(age: String) {
        _messageValidationAge.value = age.isAgeValid()
    }

    fun validateGender(gender: String) {
        _messageValidationGender.value = gender.isGenderValid()
    }

    fun validateEmail(email: String) {
        _messageValidationEmail.value = email.isEmailValid()
    }

}