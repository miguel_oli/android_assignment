package com.miguel.onboarding.presentation.splashscreen

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.R
import com.miguel.onboarding.databinding.SplashScreenFragmentBinding
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class SplashScreenFragment : DaggerFragment() {

    @Inject
    lateinit var viewModel: SplashScreenViewModel

    private lateinit var _binding: SplashScreenFragmentBinding
    private val binding get() = _binding

    private val navController: NavController by lazy { findNavController() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SplashScreenFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retrieveUser()
    }

    private fun retrieveUser() {
        getUser()
        observeRetrieveUser()
    }

    private fun getUser() = viewModel.getUserWithDelay()

    private fun observeRetrieveUser() = viewModel.retrieveUser.observe(viewLifecycleOwner) { viewState ->
        when(viewState) {
            is SplashScreenViewModel.SplashScreenStates.Error -> {
                showMessage(viewState.message)
                navigateToLogin()
            }
            is SplashScreenViewModel.SplashScreenStates.Success -> navigateToOnboardingOrProfileScreen(viewState.model)
            SplashScreenViewModel.SplashScreenStates.UserNotFound -> navigateToLogin()
        }
    }

    private fun showMessage(message: String) = Toast.makeText(
        requireContext(),
        message,
        Toast.LENGTH_SHORT
    ).show()

    private fun navigateToLogin() = navController.navigate(R.id.action_splashScreenFragment_to_loginScreenFragment)

    private fun navigateToOnboardingOrProfileScreen(user: UserModel) {
        if(user.hasDoneOnboarding) {
            navController.navigate(R.id.action_splashScreenFragment_to_profileScreenFragment)
        } else {
            navController.navigate(R.id.action_splashScreenFragment_to_onboardingScreenFragment)
        }
    }

}