package com.miguel.onboarding.presentation.loginscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miguel.core.domain.models.UserModel
import com.miguel.core.extensions.isEmailValid
import com.miguel.core.extensions.isPasswordValid
import com.miguel.onboarding.domain.usecases.SaveUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginScreenViewModel
    @Inject constructor(private val saveUserUseCase: SaveUserUseCase)
        : ViewModel() {

    sealed class LoginScreenStates {
        class Success(val model: UserModel): LoginScreenStates()
        class Error(val message: String): LoginScreenStates()
    }

    private val _saveUser = MutableLiveData<LoginScreenStates>()
    val saveUser: LiveData<LoginScreenStates> get() = _saveUser

    private val _messageValidationEmail = MutableLiveData<String?>()
    val messageValidationEmail: LiveData<String?> get() = _messageValidationEmail

    private val _messageValidationPassword = MutableLiveData<String?>()
    val messageValidationPassword: LiveData<String?> get() = _messageValidationPassword

    fun saveUser(
        email: String,
        password: String,
    ) = viewModelScope.launch {
        if(validateFields(email, password)) {
            val user = UserModel(
                email = email,
                password = password,
            )

            _saveUser.value = when (val result = saveUserUseCase(user)) {
                is ResultUserUseCase.Error -> LoginScreenStates.Error(result.message)
                is ResultUserUseCase.Success -> LoginScreenStates.Success(result.model)
                ResultUserUseCase.UserNotFound -> LoginScreenStates.Error("User not found.")
            }
        }
    }

    private fun validateFields(
        email: String,
        password: String,
    ): Boolean {
        validateEmail(email)
        validatePassword(password)

        return _messageValidationEmail.value == null && _messageValidationPassword.value == null
    }

    fun validateEmail(email: String) {
        _messageValidationEmail.value = email.isEmailValid()
    }

    fun validatePassword(password: String) {
        _messageValidationPassword.value = password.isPasswordValid()
    }

}