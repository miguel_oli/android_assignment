package com.miguel.onboarding.presentation.di

import com.miguel.onboarding.presentation.loginscreen.LoginScreenFragment
import com.miguel.onboarding.presentation.onboardingscreen.OnboardingScreenFragment
import com.miguel.onboarding.presentation.profilescreen.ProfileScreenFragment
import com.miguel.onboarding.presentation.splashscreen.SplashScreenFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentsModule {

    @ContributesAndroidInjector
    fun provideSplashScreenFragment(): SplashScreenFragment

    @ContributesAndroidInjector
    fun provideLoginScreenFragment(): LoginScreenFragment

    @ContributesAndroidInjector
    fun provideOnboardingScreenFragment(): OnboardingScreenFragment

    @ContributesAndroidInjector
    fun provideProfileScreenFragment(): ProfileScreenFragment

}