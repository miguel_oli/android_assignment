package com.miguel.onboarding.presentation.splashscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miguel.core.domain.models.UserModel
import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashScreenViewModel
    @Inject constructor(private val getUserUseCase: GetUserUseCase)
    : ViewModel() {

    sealed class SplashScreenStates {
        class Success(val model: UserModel): SplashScreenStates()
        class Error(val message: String): SplashScreenStates()
        object UserNotFound: SplashScreenStates()
    }

    private val _retrieveUser = MutableLiveData<SplashScreenStates>()
    val retrieveUser: LiveData<SplashScreenStates> get() = _retrieveUser

    fun getUser() = viewModelScope.launch {
        _retrieveUser.value = when(val result = getUserUseCase()) {
            is ResultUserUseCase.Error -> SplashScreenStates.Error(result.message)
            is ResultUserUseCase.Success -> SplashScreenStates.Success(result.model)
            ResultUserUseCase.UserNotFound -> SplashScreenStates.UserNotFound
        }
    }

    fun getUserWithDelay() = viewModelScope.launch {
        delay(1000)
        getUser()
    }
}