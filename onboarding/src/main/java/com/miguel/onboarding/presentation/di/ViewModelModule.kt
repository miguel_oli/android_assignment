package com.miguel.onboarding.presentation.di

import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.SaveUserUseCase
import com.miguel.onboarding.presentation.loginscreen.LoginScreenViewModel
import com.miguel.onboarding.presentation.onboardingscreen.OnboardingScreenViewModel
import com.miguel.onboarding.presentation.profilescreen.ProfileScreenViewModel
import com.miguel.onboarding.presentation.splashscreen.SplashScreenViewModel
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule {

    @Provides
    fun provideLoginScreenViewModel(saveUserUseCase: SaveUserUseCase): LoginScreenViewModel =
        LoginScreenViewModel(saveUserUseCase)

    @Provides
    fun provideSplashScreenViewModel(getUserUseCase: GetUserUseCase): SplashScreenViewModel =
        SplashScreenViewModel(getUserUseCase)

    @Provides
    fun provideProfileScreenViewModel(getUserUseCase: GetUserUseCase): ProfileScreenViewModel =
        ProfileScreenViewModel(getUserUseCase)

    @Provides
    fun provideOnboardingScreenViewModel(
        getUserUseCase: GetUserUseCase,
        saveUserUseCase: SaveUserUseCase
    ): OnboardingScreenViewModel = OnboardingScreenViewModel(getUserUseCase, saveUserUseCase)

}