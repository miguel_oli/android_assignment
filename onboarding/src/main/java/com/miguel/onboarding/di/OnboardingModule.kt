package com.miguel.onboarding.di

import com.miguel.onboarding.domain.di.UseCaseModule
import com.miguel.onboarding.presentation.di.FragmentsModule
import com.miguel.onboarding.presentation.di.ViewModelModule
import dagger.Module

@Module(
    includes = [
        UseCaseModule::class,
        ViewModelModule::class,
        FragmentsModule::class,
    ]
)
class OnboardingModule