package com.miguel.onboarding.domain.usecases

import com.miguel.core.domain.models.UserModel
import com.miguel.core.domain.repositories.contract.ILocalUserRepository
import com.miguel.core.domain.repositories.result.ResultUserRepository
import com.miguel.onboarding.domain.usecases.result.ResultUserUseCase
import javax.inject.Inject

class SaveUserUseCase
    @Inject constructor(private val localUserRepository: ILocalUserRepository) {

    suspend operator fun invoke(user: UserModel) =
        when(val result = localUserRepository.save(user)) {
            is ResultUserRepository.Error -> ResultUserUseCase.Error(result.message)
            is ResultUserRepository.Success -> ResultUserUseCase.Success(result.model)
            ResultUserRepository.UserNotFound -> ResultUserUseCase.UserNotFound
        }
}