package com.miguel.onboarding.domain.di

import com.miguel.core.domain.repositories.contract.ILocalUserRepository
import com.miguel.onboarding.domain.usecases.GetUserUseCase
import com.miguel.onboarding.domain.usecases.SaveUserUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Provides
    @Singleton
    fun provideGetUserUseCase(repository: ILocalUserRepository): GetUserUseCase =
        GetUserUseCase(repository)

    @Provides
    @Singleton
    fun provideSaveUserUseCase(repository: ILocalUserRepository): SaveUserUseCase =
        SaveUserUseCase(repository)

}