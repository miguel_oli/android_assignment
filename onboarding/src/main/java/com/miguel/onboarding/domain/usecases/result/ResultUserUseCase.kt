package com.miguel.onboarding.domain.usecases.result

import com.miguel.core.domain.models.UserModel

sealed class ResultUserUseCase {
    class Success(val model: UserModel): ResultUserUseCase()
    object UserNotFound: ResultUserUseCase()
    class Error(val message: String): ResultUserUseCase()
}
