package com.miguel.core.extensions

fun String?.isEmailValid(): String? {
    val emailRegex = "^\\S+@\\S+\\.\\S+$".toRegex()

    val isEmailValid = emailRegex.matches(this ?: "")
    if(!isEmailValid)
        return "Invalid e-mail"

    return null
}

fun String?.isPasswordValid(): String? {
    val isPasswordSizeValid = this?.length ?: 0 >= 6
    if(!isPasswordSizeValid)
        return "Password needs to be at least 6 characters"

    return null
}

fun String?.isFirstNameValid(): String? {
    val isFirstNameSizeValid = this?.length ?: 0 >= 3
    if(!isFirstNameSizeValid)
        return "First name needs to be at least 3 characters"

    return null
}

fun String?.isLastNameValid(): String? {
    val isFirstNameSizeValid = this?.length ?: 0 >= 3
    if(!isFirstNameSizeValid)
        return "Last name needs to be at least 3 characters"

    return null
}

fun String?.isAgeValid(): String? {
    val isAgeSizeValid = this?.length ?: 0 >= 1
    if(!isAgeSizeValid)
        return "Age shouldn't be empty"

    val isAgeValueValid = this?.toIntOrNull() ?: 0 >= 1
    if(!isAgeValueValid)
        return "Age shouldn't be less than 1"

    return null
}

fun String?.isGenderValid(): String? {
    val isGenderSizeValid = this?.length ?: 0 >= 4
    if(!isGenderSizeValid)
        return "Gender needs to be at least 3 characters"

    return null
}