package com.miguel.core.extensions

import com.miguel.core.data.entities.UserEntity
import com.miguel.core.domain.models.UserModel

fun UserEntity.toModel() = UserModel(
    firstName = firstName,
    lastName = lastName,
    age = age,
    gender = gender,
    country = country,
    email = email,
    password = password,
    hasDoneOnboarding = hasDoneOnboarding,
)

fun UserModel.toEntity() = UserEntity(
    firstName = firstName,
    lastName = lastName,
    age = age,
    gender = gender,
    country = country,
    email = email,
    password = password,
    hasDoneOnboarding = hasDoneOnboarding,
)