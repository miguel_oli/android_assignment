package com.miguel.core.di

import android.content.Context
import android.content.SharedPreferences
import com.miguel.core.data.datasources.keys.SharedPreferencesKeys
import com.miguel.core.data.di.DataModule
import com.miguel.core.domain.di.RepositoryModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        DataModule::class,
        RepositoryModule::class,
    ]
)
class CoreModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences = context.getSharedPreferences(
        SharedPreferencesKeys.SHP_FILE_KEY, Context.MODE_PRIVATE
    )

}