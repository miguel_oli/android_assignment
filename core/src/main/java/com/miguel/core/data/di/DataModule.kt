package com.miguel.core.data.di

import android.content.Context
import android.content.SharedPreferences
import com.miguel.core.data.datasources.LocalUserDataSourceImplementation
import com.miguel.core.data.datasources.contract.ILocalUserDataSource
import com.miguel.core.data.datasources.keys.SharedPreferencesKeys
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideUserDataSource(database: SharedPreferences): ILocalUserDataSource =
        LocalUserDataSourceImplementation(database)

}