package com.miguel.core.data.entities

data class UserEntity(
    val firstName: String,
    val lastName: String,
    val age: Int,
    val gender: String,
    val country: String,
    val email: String,
    val password: String,
    val hasDoneOnboarding: Boolean,
)
