package com.miguel.core.data.datasources.keys

object SharedPreferencesKeys {

    const val SHP_FILE_KEY = "com.miguel.androidassingment.sharedpreferences"
    const val USER_KEY = "USER_KEY"

}