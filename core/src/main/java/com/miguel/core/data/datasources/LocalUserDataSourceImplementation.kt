package com.miguel.core.data.datasources

import android.content.SharedPreferences
import com.google.gson.Gson
import com.miguel.core.data.datasources.contract.ILocalUserDataSource
import com.miguel.core.data.datasources.keys.SharedPreferencesKeys.USER_KEY
import com.miguel.core.data.entities.UserEntity
import javax.inject.Inject

class LocalUserDataSourceImplementation
    @Inject constructor(private val database: SharedPreferences)
        : ILocalUserDataSource {

    override suspend fun get(): UserEntity {
        val json = database.getString(USER_KEY, "")
        if(json.isNullOrEmpty())
            throw NoSuchElementException()
        return Gson().fromJson(json, UserEntity::class.java)
    }

    override suspend fun save(user: UserEntity): UserEntity {
        val json = Gson().toJson(user)
        database.edit().putString(USER_KEY, json).apply()
        return user
    }

}