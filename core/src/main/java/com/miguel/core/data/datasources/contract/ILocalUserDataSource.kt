package com.miguel.core.data.datasources.contract

import com.miguel.core.data.entities.UserEntity

interface ILocalUserDataSource {

    suspend fun get(): UserEntity

    suspend fun save(user: UserEntity): UserEntity

}