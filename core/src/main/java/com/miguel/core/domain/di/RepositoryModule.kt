package com.miguel.core.domain.di

import com.miguel.core.data.datasources.contract.ILocalUserDataSource
import com.miguel.core.domain.repositories.LocalUserRepositoryImplementation
import com.miguel.core.domain.repositories.contract.ILocalUserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideUserRepository(datasource: ILocalUserDataSource): ILocalUserRepository =
        LocalUserRepositoryImplementation(datasource)

}