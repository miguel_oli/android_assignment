package com.miguel.core.domain.repositories

import com.miguel.core.data.datasources.contract.ILocalUserDataSource
import com.miguel.core.domain.models.UserModel
import com.miguel.core.domain.repositories.contract.ILocalUserRepository
import com.miguel.core.domain.repositories.result.ResultUserRepository
import com.miguel.core.extensions.toEntity
import com.miguel.core.extensions.toModel
import java.lang.Exception
import javax.inject.Inject

class LocalUserRepositoryImplementation
    @Inject constructor(private val datasource: ILocalUserDataSource)
        : ILocalUserRepository {

    override suspend fun get(): ResultUserRepository =
        try {
            val result = datasource.get()
            ResultUserRepository.Success(result.toModel())
        } catch (e: NoSuchElementException) {
            ResultUserRepository.UserNotFound
        } catch (e: Exception) {
            ResultUserRepository.Error(e.localizedMessage.orEmpty())
        }

    override suspend fun save(user: UserModel): ResultUserRepository =
        try {
            val result = datasource.save(user.toEntity())
            ResultUserRepository.Success(result.toModel())
        } catch (e: Exception) {
            ResultUserRepository.Error(e.localizedMessage.orEmpty())
        }

}