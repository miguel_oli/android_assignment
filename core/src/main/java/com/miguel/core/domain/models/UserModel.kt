package com.miguel.core.domain.models

data class UserModel(
    val firstName: String = "",
    val lastName: String = "",
    val age: Int = 0,
    val gender: String = "",
    val country: String = "",
    val email: String = "",
    val password: String = "",
    val hasDoneOnboarding: Boolean = false,
)
