package com.miguel.core.domain.repositories.result

import com.miguel.core.domain.models.UserModel

sealed class ResultUserRepository {
    class Success(val model: UserModel): ResultUserRepository()
    object UserNotFound: ResultUserRepository()
    class Error(val message: String): ResultUserRepository()
}
