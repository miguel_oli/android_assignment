package com.miguel.core.domain.repositories.contract

import com.miguel.core.domain.models.UserModel
import com.miguel.core.domain.repositories.result.ResultUserRepository

interface ILocalUserRepository {

    suspend fun get(): ResultUserRepository

    suspend fun save(user: UserModel): ResultUserRepository

}