package com.miguel.core.domain.repositories

import com.miguel.core.data.datasources.contract.ILocalUserDataSource
import com.miguel.core.data.entities.UserEntity
import com.miguel.core.domain.models.UserModel
import com.miguel.core.domain.repositories.result.ResultUserRepository
import com.miguel.core.extensions.toEntity
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class LocalUserRepositoryImplementationTest {

    private lateinit var datasource: ILocalUserDataSource
    private lateinit var repo: LocalUserRepositoryImplementation

    @Before
    fun setUp() {
        datasource = mockk()
        repo = LocalUserRepositoryImplementation(datasource)
    }

    @Test
    fun `getting user successfully`() = runBlocking {
        val user = UserEntity(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        coEvery { datasource.get() } returns user

        val result = repo.get()

        coVerify { datasource.get() }
        assertThat(result, instanceOf(ResultUserRepository.Success::class.java))
    }

    @Test
    fun `getting user with not found error`() = runBlocking {
        coEvery { datasource.get() }.throws(NoSuchElementException())

        val result = repo.get()

        coVerify { datasource.get() }
        assertEquals(result, ResultUserRepository.UserNotFound)
    }

    @Test
    fun `getting user with generic error`() = runBlocking {
        val message = "generic error"
        coEvery { datasource.get() }.throws(Exception(message))

        val result = repo.get()

        coVerify { datasource.get() }
        assertThat(result, instanceOf(ResultUserRepository.Error::class.java))
    }

    @Test
    fun `saving user successfully`() = runBlocking {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        val userEntity = user.toEntity()
        coEvery { datasource.save(userEntity) } returns userEntity

        val result = repo.save(user)

        coVerify { datasource.save(userEntity) }
        assertThat(result, instanceOf(ResultUserRepository.Success::class.java))
    }

    @Test
    fun `saving user with error`() = runBlocking {
        val user = UserModel(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        val userEntity = user.toEntity()
        val message = "generic error"
        coEvery { datasource.save(userEntity) }.throws(Exception(message))

        val result = repo.save(user)

        coVerify { datasource.save(userEntity) }
        assertThat(result, instanceOf(ResultUserRepository.Error::class.java))
    }

}