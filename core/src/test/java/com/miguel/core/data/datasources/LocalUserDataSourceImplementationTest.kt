package com.miguel.core.data.datasources

import android.content.SharedPreferences
import com.google.gson.Gson
import com.miguel.core.data.datasources.keys.SharedPreferencesKeys.USER_KEY
import com.miguel.core.data.entities.UserEntity
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class LocalUserDataSourceImplementationTest {
    private lateinit var pref: SharedPreferences
    private lateinit var datasource: LocalUserDataSourceImplementation
    @Before
    fun setUp() {
        pref = mockk()
        datasource = LocalUserDataSourceImplementation(pref)
    }

    @Test(expected = NoSuchElementException::class)
    fun `getting empty user`() = runBlocking {
        coEvery { pref.getString(USER_KEY, "") } returns ""
        datasource.get()
        return@runBlocking
    }

    @Test
    fun `getting user successfully`() = runBlocking {
        val json = """
            {
                "firstName": "José",
                "lastName": "Silva",
                "age": 25,
                "gender": "Male",
                "country": "Brazil",
                "email": "teste@gmail.com",
                "password": "123456"
            }
        """
        val user = Gson().fromJson(json, UserEntity::class.java)

        coEvery { pref.getString(USER_KEY, "") } returns json
        val result = datasource.get()

        coVerify { pref.getString(USER_KEY, "") }
        assertEquals(user, result)
    }

    @Test
    fun `saving user successfully`() = runBlocking {
        val user = UserEntity(
            firstName = "José",
            lastName = "Silva",
            age = 25,
            gender ="Male",
            country = "Brazil",
            email = "teste@gmail.com",
            password = "123456",
            hasDoneOnboarding = false,
        )
        val json = Gson().toJson(user)

        coEvery { pref.edit().putString(USER_KEY, json).apply() } returns Unit
        val result = datasource.save(user)

        coVerify { pref.edit().putString(USER_KEY, json).apply() }
        assertEquals(user, result)
    }
}